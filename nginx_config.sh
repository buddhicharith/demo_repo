#!/bin/sh
cd /var/www/html/
sudo mkdir myapp
cp /home/jenkins/test/repo/* /var/www/html/myapp
chown -R $USER:$USER /var/www/html/myapp/

cp /home/jenkins/test/repo/ /etc/nginx/sites-available/myapp.conf

rm /etc/nginx/sites-enabled/default

ln -s /etc/nginx/sites-available/myapp.conf /etc/nginx/sites-enabled/

nginx -t

systemctl restart nginx
