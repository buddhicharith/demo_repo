#FROM ubuntu:20.04
FROM nginx
#Nginx Install
#RUN apt update && apt-get install nginx -y

COPY ./index.html /usr/share/nginx/html/index.html
#COPY ./.env /usr/share/nginx/html/.env

#RUN service nginx restart

#Edit Variables in .env File
#RUN cd usr/share/nginx/html
#ENV sed -i '/DB_HOST=/c\DB_HOST=192.168.20.140' .env
#ENV DB_HOST=192.168.10.5
#RUN touch /.env                                                                                                   
#RUN printenv > /.env

#Install the Site24x7 Agent
RUN apt-get update && apt-get install wget -y
#RUN apt install python3.8
RUN apt-get update && apt install python3-distutils -y
RUN apt-get update

RUN wget https://staticdownloads.site24x7.com/server/Site24x7InstallScript.sh
RUN bash Site24x7InstallScript.sh -i -key=us_61711bd46a5d76474182f17f017058d8

RUN apt-get update -y